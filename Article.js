'use strict';

import React, { Component } from 'react';

import {
  Image,
  Dimensions,
  StyleSheet,
  View,
  StatusBar,
} from 'react-native';

var window = Dimensions.get('window');

class Article extends Component {
  render() {
    return (
      
      <View style={styles.contentContrainer}>

  		<StatusBar hidden ={true} />

      	<Image
        style={styles.articleHead}
        source={require('./img/mock.jpg')}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
contentContrainer:{
	justifyContent: 'center',
    alignItems: 'center',
    flex: 1
},
articleHead:{
	width: window.width,
	height: window.height,
},

});


export default Article;