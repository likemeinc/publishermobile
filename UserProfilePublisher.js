'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  Text,
  StyleSheet,
  Image,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class UserProfilePublisher extends Component {
  render() {
    return (

      <View>	

      			{/*cover image*/}
     			  <View style={styles.cover}/>

        		{/*position absolute mock view*/}
        		<View style={styles.mockProfilePic}>
					<Image style={styles.publisherPicture}
						   source={require('./img/logo.jpg')}>
					</Image>

					<Image
						     style={styles.writterPicture}
						     source={require('./img/profile.jpg')} />
        		</View>
        		
        		{/*history writter*/}
				    <View style={styles.writterHistory}>

					<View>
			    		<View style={styles.firstRow}>
							<Text style={styles.recommended}>
		  						
							</Text>
							<Text/>
							<Text style={styles.writed}>
		  						
							</Text>
			    		</View>

			    		<View style={styles.secondRow}>
							<Text style={styles.textRecommended}>
		  						
							</Text>
							<Text/> 	
							<Text style={styles.textWrited}>
		  							
							</Text>
			     		</View>
					</View>

					<View style={styles.writterName}>
							<Text style={{fontSize:18}}>
								Vanessa D Clipton
							</Text>
					</View>

					<View style={styles.writterDescription}>
							<Text style={{fontSize:14,color:'rgb(38,38,38)'}}>
								Writer's of ..........................
							</Text>
					</View>
				    </View>
					
				    {/*layout height:8 */}
				    <View style={styles.singleLine}/> 

					<View style={styles.writing}>


					<Text>Lorem 
					<Icon name="ios-time" size={30}  color="#99BB99"/>
					<Icon name="ios-person" size={50} color="#4F8EF7" />
					<Icon name="ios-link" size={50} color="#4F8EF7" /> 

					Ipsum</Text>


					</View>

      </View>
    );
  }

}

var window = Dimensions.get('window');

const styles = StyleSheet.create({

	contrainer:{
		backgroundColor:'#dddddd',
	},
	cover:{
		zIndex:1,
		height:65,
		width: window.width,
    	borderBottomWidth:1,
    	borderColor:'#e6e6e6',
	},
	writterHistory:{
		zIndex:1,
		flexDirection: 'column',
    justifyContent: 'space-between',
		height:150,
		width: window.width,

	},
	publisherPicture:{
		zIndex:1,
		borderRadius:8,
		width:100,
		height:100,
		borderWidth:2,
		borderColor:'#dddddd',
	},
	writterPicture:{
		zIndex:6,
		width:48,
		height:48,
		borderRadius:24,
		borderWidth:2,
		borderColor:'#dddddd',
		bottom:40,
		left:36,
	},
  	singleLine:{
    	marginTop:10,
    	height:10,
    	width:window.width,
    	borderWidth:1,
    	borderColor:'#e6e6e6',
    	backgroundColor:'#f2f2f2',
  },
  recommended:{
  		textAlign:'center',
  		fontSize:24,
		marginTop:10,
 		width:102,
  },
  writed:{
  		textAlign:'center',
  		fontSize:24,
		marginTop:10,
 		width:98,	
  },
  firstRow:{
    	flexDirection: 'row',
   	 	justifyContent: 'space-between',
  },
  secondRow:{
    	flexDirection: 'row',
    	justifyContent: 'space-between',
  },
  textRecommended:{
  		marginLeft:12,
  		fontSize:14,
  		color:'#8C8887',
  },
  textWrited:{
  		fontSize:14,
  		color:'#8C8887',
  		marginRight:30,
  },

  writterDescription:{
  		paddingRight:16,
  		paddingLeft:16,
      	marginBottom:15,
      	alignItems:'center',
  },
  writterName:{
  		marginTop:43.5,
  		alignItems:'center',
  },
  mockProfilePic:{
  		zIndex:4,
  		height:0,
		width: window.width,
		alignItems: 'center',
		backgroundColor: '#DDFFDD',
		position: 'absolute',
		top: 31,
  },
});

export default UserProfilePublisher;