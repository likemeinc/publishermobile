import React, { Component } from 'react';

import {
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  PixelRatio,
  Dimensions,
} from 'react-native';
import GridView from 'react-native-grid-view' //import custom module GridView

var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

class PublisherList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: null,
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: responseData.movies,
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (

      <GridView
      style={styles.gridView}
        items={this.state.dataSource}
        itemsPerRow={2}
        renderItem={this.renderContent}
        showsVerticalScrollIndicator={false}
      />
      
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.indicator}>
      <ActivityIndicator
          color="blue"
          size="small"
        />
      </View>
    );
  }
    
  renderContent(content) {
    return (

      <TouchableOpacity>
    	   <View style={styles.contentContrainer}>
    		  <Image
    		    style={styles.publisherLogo}
    		    source={require('./img/logo.jpg')} />

    		    <Text style={styles.publisherName}>
  					   infoGraphic Thailand
    		    </Text>

    		    <Text style={styles.columnName}>
    		     Money Idea.
    		    </Text>
    	   </View>
      </TouchableOpacity>
      
    );
  }
}


var window = Dimensions.get('window');
const a = PixelRatio.get();
const dp = PixelRatio.getPixelSizeForLayoutSize(180);

const styles = StyleSheet.create({

	indicator:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
		
	},
	contentContrainer:{
		height:180, //180
		width:163, //163
		backgroundColor:'#FFFFFF',
		borderRadius:5,
		borderColor:'#E5E5E5',
		borderWidth:0.7,
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',
		marginLeft:5,
		marginRight:5,
		marginTop:5,
		marginBottom:5,
	},

	publisherLogo:{
		height:100,
		width:100,
		borderRadius:10,
	},
	publisherName:{
		marginTop:9,
		marginBottom:3,
		fontSize:14,
		fontWeight:'bold',
    textAlign:'center',

	},
	columnName:{
		fontSize:12,
		color:'#999999'
	},


});

export default PublisherList;