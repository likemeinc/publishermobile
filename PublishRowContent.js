'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

class PublishRowContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.movies),
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      
      <ListView
        horizontal={true}
        dataSource={this.state.dataSource}
        renderRow={this.renderContent}/>
      
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.indicator}>
      <ActivityIndicator
          color="blue"
          size="small"
        />
      </View>
    );
  }
    
  renderContent(content) {
    return (

    <View>

        {/*Content and read duration */}
        <View style={styles.column}>

          <TouchableOpacity>
            <Image 
            style={styles.rowContent}
            source={require('./img/mock.jpg')}>

              <View style={styles.textContent}>
              
                <Image
                style={styles.publisherLogo}
                source={require('./img/publisherLogo.png')} />
                <Text style={styles.descriptionContent}>

                 "Sed ut perspiciatis unde omnis iste natus error unde omnis iste"
                </Text>
              </View>
            </Image> 
          </TouchableOpacity>

            <Text style={styles.readDuration}>
               {content.year} min read
            </Text>
        </View>

    </View>

    );
  }
}

const styles = StyleSheet.create({

  column:{
    flex:1,
    flexDirection:'column',
  },
  row:{
    flex:1,
    flexDirection:'row',
  },  
  rowContent:{
    marginLeft:4.5,
    marginRight:4.5,
    width: 122,
    height: 178,
    borderRadius: 10, 
  },
	readDuration:{
    fontSize:12,
    color:'#434343',
    marginLeft:8,
    marginTop:10,
    textAlign: 'center',
  },
  textContent:{
    width: 122,
    height: 178,
    justifyContent:'space-between',
    paddingTop:11,
    backgroundColor:'transparent',
  },
  publisherLogo:{
    alignSelf:'center',
    height:19,
    width:107,
    resizeMode:'contain',
  },
  descriptionContent:{
    paddingLeft:10,
    paddingRight:12,
    fontSize:14,
    color:'#FFFFFF',
    marginBottom:11,
    fontWeight:'bold',
  },

indicator:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },

});

export default PublishRowContent;
