/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  StyleSheet,
  View,
} from 'react-native';
import UserProfile from './UserProfile';
import ScrollableTabView, { DefaultTabBar,ScrollableTabBar } from 'react-native-scrollable-tab-view';
import UserProfilePublisher from './UserProfilePublisher';
import FirstFeedRoute from './FirstFeedRoute';
import FirstPublisherRoute from './FirstPublisherRoute';

class Publisher extends Component {

  render() {

    return (

              <ScrollableTabView style={{marginTop:20}}
                                 tabBarBackgroundColor='#F7F7F7'
                                 locked={true}
                                 tabBarUnderlineColor='#FFFFFFFF'
                                 scrollWithoutAnimation={true}
                                 tabBarPosition='bottom'
                                 initialPage={0}
                                 tabBarActiveTextColor='#434343'
                                 tabBarInactiveTextColor='rgba(67, 67, 67,0.3)'
                                 renderTabBar={() => <ScrollableTabBar/>}>

                 <UserProfilePublisher tabLabel="Test"/>
                 <FirstFeedRoute tabLabel="Feed" />
                 <FirstPublisherRoute tabLabel="Publisher"/>
                 <UserProfile tabLabel="Me" />
                 
                 {/*<Publish tabLabel="Publisher" />
                 <PublishColumn tabLabel="column"/>
                 <SimpleNavigationApp tabLabel="Article"/>*/}
              </ScrollableTabView>
    );
  }
}


const styles = StyleSheet.create({

  actionBar:{
    width:window.width,
  },

});
AppRegistry.registerComponent('Publisher', () => Publisher);