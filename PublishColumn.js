'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  View,
} from 'react-native';
import ColumnContent from './ColumnContent';

class PublishColumn extends Component {

  render() {
    return (

      		<ScrollView>
      			{/*cover image*/}
     		   	<View >
     			  	<Image 
						    style={styles.cover}
						    source={require('./img/mockCover.jpg')}>
					    </Image> 		
				    </View>


        		{/*position absolute mock view*/}
        		<View style={styles.mockProfilePic}>
					    <Image 
						  style={styles.publisherLogo}
						  source={require('./img/profile.jpg')}/> 

						    <View style={styles.flexColumnHeader}>
							     <Text style={styles.editorialPicked}>
							       Editorial Picked
							     </Text>

							     <Text style={styles.publisherName}>
							       by Infographic Thailand
							     </Text>
						    </View>
        		</View>

        		<View style={styles.columnDescription}>
        			<Text style={styles.description}>
        		  		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
        			</Text>
        		</View>

        		<ColumnContent/>

        	</ScrollView>
        		
      
    );
  }
}

var window = Dimensions.get('window');

const styles = StyleSheet.create({

	contrainer:{
		backgroundColor:'#dddddd',
	},
	publisherName:{
		backgroundColor: 'transparent',
  	color:'rgb(67,67,67)',
  	fontSize:15,
  	marginBottom:19,
  	marginTop:10,
	},
	editorialPicked:{
  	backgroundColor: 'transparent',
  	color:'#FFFFFF',
  	fontSize:20,
  	fontWeight:'bold',
  	marginTop:18,
  	marginBottom:10,
  	},
	flexColumnHeader:{
		flex:1,
		flexDirection:'column',
		paddingLeft:20,
	},
	cover:{
		height:130,
		width: window.width,
    borderBottomWidth:1,
    borderColor:'#e6e6e6',
    justifyContent:'flex-end',
    alignItems:'center',
	},
	publisherLogo:{
		borderRadius:10,
		width:100,
		height:100,
		borderWidth:2,
		borderColor:'#dddddd',
	},
  mockProfilePic:{
  	flex:1,
  	flexDirection:'row',
  	zIndex:4,
  	height: 100,
		width: window.width,
		alignItems: 'flex-start',
		backgroundColor: 'transparent',
		position: 'absolute',
		top: 80,
		left:20,
  },
  columnDescription:{
  	marginTop:78,
  	paddingLeft:20,
  	paddingRight:20,
  	marginBottom:20,
  },
  description:{
  	fontSize:14,
  	color:'rgb(38,38,38)',
  },
});

export default PublishColumn;