
'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import RowContent from './RowContent';

var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

class FeedList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.movies),
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderContent}
        style={styles.listView}
        showsVerticalScrollIndicator={false}/>
      
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.indicator}>
      <ActivityIndicator
          color="blue"
          size="small"
        />
      </View>
    );
  }

  renderContent(content) {
    return (

    <View>

      {/*content*/}
      <View style={styles.container}>

        <View style={styles.rowContainer}>
          <Text style={styles.editorial}>{content.title}</Text>       
          <Text style={styles.by}> by </Text>   
          <Text style={styles.publisher}>{content.year}</Text>
          <TouchableOpacity>
              <Text style={styles.more}>More</Text>
          </TouchableOpacity>
          

        </View>

        <View style={styles.description}>
          <Text style={styles.textDescription} 
                numberOfLines={2}>
            "Sed ut perspiciatis unde omnis iste natus error unde omnis iste natus error sit voluptatem accusantium doloremque laudantium"
          </Text>
        </View>

        <ScrollView style={styles.contentView}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>

          <View style={styles.contentShift}>
              <RowContent/>
          </View>

        </ScrollView> 

      </View>

      {/*Mock Layout*/}
      <View style={styles.mockLayout}></View>
      
    </View>

    );
  }

}

var window = Dimensions.get('window');
var styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    borderTopWidth:1,
    borderBottomWidth:1,
    backgroundColor:'#FFFFFF',
    borderColor:'#DDDDDD',
  },
  rowContainer: {
    width:window.width,
    marginTop:23,
    paddingLeft:20,
    paddingRight:20,
    flexDirection: 'row',
  },
  by: {
    color:'#434343',
    fontSize: 15,
    textAlign: 'left',
  },
  editorial: {
    color:'#434343',
    fontWeight: 'bold',
    fontSize: 15,
  },
  publisher: {
    color:'#434343',
    fontSize: 15,
  },
  more:{
    fontSize:14,
    textAlign:'right',
  },
  textDescription:{
    fontSize:14, 
    color:'#434343',
    textAlign: 'left', 
  },
  description: {
    paddingLeft:20,
    marginRight:20,
    marginTop:7,
  },
  contentTitle:{
    fontSize: 20,
    textAlign: 'center',
    fontWeight:'bold',
    color:'#FFFFFF'
  },
  contentDescription:{
    flex:1,
    fontSize:12,
    color:'#FFFFFF',
    fontWeight:'bold',
  },
  contentShift:{
    marginBottom:23,
    marginTop:13,
    marginLeft:14.5,
    marginRight:14.5,
  },
  listView: {
    backgroundColor: '#f2f2f2',
  },
  mockLayout:{              //mock position absolute
    height:8,
    width:window.width,
    backgroundColor:'rgb(247,247,247)',
  },
  indicator:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
});

export default FeedList;