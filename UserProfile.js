'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  Text,
  StyleSheet,
  Image,
  View,
  ScrollView,
  PixelRatio,
} from 'react-native';

class UserProfile extends Component {
  render() {
    return (

      
          <View style={styles.contrainer}>
      			     {/*cover image*/}
     			      <View style={styles.cover}/>

        		     {/*position absolute mock view*/}
        		    <View style={styles.mockProfilePic}>
					         <Image 
						          style={styles.writterPicture}
						          source={require('./img/profile.jpg')}/> 
        		    </View>
        		
        		      {/*history writter*/}
				        <View style={styles.writterHistory}>


		            <View style={styles.writterName}>
							     <Text style={{fontSize:18}}>
								      Vanessa D Clipton
							     </Text>
					       </View>

					       <View style={styles.writterDescription}>
							     <Text style={{fontSize:14,color:'rgb(38,38,38)'}} numberOfLines={2}>
								      Lorem ipsum dolor sit amet,  quis nostrud exercitation ullamco. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							     </Text>
					       </View>

				    </View>
					
            {/*layout height:8 */} 
				    <View style={styles.singleLine}/>
				    
            <ScrollView style={{flex:1}}> 

            
            <View style={styles.menu}><Text style={{fontSize:14}}>Bookmark</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>Reading History</Text></View>
            <View style={styles.singleLine}/>

            <View style={styles.menuHead}><Text style={{fontSize:14,fontWeight:'bold'}}>PUBLISHER</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>mock Aommoney</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>mock Aommoney</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>mock Aommoney</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>mock Aommoney</Text></View>  
            <View style={styles.singleLine}/>

            <View style={styles.menuHead}><Text style={{fontSize:14,fontWeight:'bold'}}>OTHER</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>Setting</Text></View>
            <View style={styles.menu}><Text style={{fontSize:14}}>Logout</Text></View>
            <View style={styles.singleLine}/>
            </ScrollView>
          </View>
      
    );
  }
}

var window = Dimensions.get('window');

const styles = StyleSheet.create({

	contrainer:{
    flex:1,
		backgroundColor:'#f2f2f2',
	},
	cover:{
		zIndex:1,
		height:65,
		width: window.width,
    borderBottomWidth:1,
    borderColor:'#e6e6e6',
    backgroundColor:'#FFFFFF',
	},
	writterHistory:{
		zIndex:1,
		flexDirection: 'column',
    justifyContent: 'space-between',
		height:190,
		width: window.width,
    backgroundColor:'#FFFFFF',

	},
	writterPicture:{
		borderRadius:64,
		width:128,
		height:128,
		borderWidth:2,
		borderColor:'#dddddd',
	},
  	singleLine:{
    	height:10,
    	width:window.width,
    	borderTopWidth:1,
    	borderColor:'#e6e6e6',
    	backgroundColor:'#f2f2f2',
  },
  writterDescription:{
  		paddingRight:16,
  		paddingLeft:16,
      marginBottom:15,
  },
  writterName:{
  		marginTop:114,
  		alignItems:'center',
  },
  mockProfilePic:{
  	zIndex:4,
  	height: 0,
		width: window.width,
		alignItems: 'center',
		backgroundColor: '#DDFFDD',
		position: 'absolute',
		top: 31,
  },
  menu:{
    height:45,
    width:window.width,
    justifyContent:'center',
    paddingLeft:30.8,
    borderBottomWidth:1,
    borderColor:'rgb(229,229,229)',
    backgroundColor:'#FFFFFF',

  },
  menuHead:{
    paddingLeft:19,
    justifyContent:'center',
    height:38,
    backgroundColor:'#FFFFFF',
  },
});

export default UserProfile;