'use strict';

import React, { Component } from 'react';

import {
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import GridView from 'react-native-grid-view' //import custom module GridView

var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

class ColumnContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: null,
      loaded: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: responseData.movies,
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (

      <GridView
      style={styles.gridView}
        items={this.state.dataSource}
        itemsPerRow={2}
        renderItem={this.renderContent}
      />
      
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.indicator}>
      <ActivityIndicator
          color="blue"
          size="small"
        />
      </View>
    );
  }
    
  renderContent(content) {
    return (

    <View>

        {/*Content and read duration */}
        <View style={styles.column}>

          <TouchableOpacity>
             <Image 
                style={styles.rowContent}
                source={require('./img/mock.jpg')}>

              <View style={styles.textContent}>

                <Image
                style={styles.publisherLogo}
                source={require('./img/publisherLogo.png')} />

                <Text style={styles.descriptionContent}>
                 "Lorem ipsum dolor sit amet, consectetur adipiscing"
                </Text>

              </View>

            </Image> 
          </TouchableOpacity>

            <Text style={styles.readDuration}>
               {content.year} min read
            </Text>
        </View>

    </View>

    );
  }
}

const styles = StyleSheet.create({

  column:{
    flex:1,
    flexDirection:'column',
  },
  row:{
    flex:1,
    flexDirection:'row',
  },  
  rowContent:{
    marginLeft:6,
    marginRight:6,
    width: 162, //162
    height: 239, //239
    borderRadius: 10, 
  },
	readDuration:{
    fontSize:12,
    color:'#434343',
    marginLeft:8,
    marginTop:10,
    textAlign: 'left',
    marginBottom:21,
  },
  textContent:{
    width: 162,
    height: 239,
    alignItems:'center',
    justifyContent:'space-between',
    backgroundColor:'transparent',
  },
  publisherLogo:{
    marginTop:14.8,
    alignSelf:'center',
    height:25,
    width:141,
    resizeMode:'contain',
  },
  descriptionContent:{
    paddingLeft:13.4,
    paddingRight:16.6,
    marginBottom:17,
    fontSize:18,
    color:'#FFFFFF',
    fontWeight:'bold',
  },
  indicator:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },

});

export default ColumnContent;