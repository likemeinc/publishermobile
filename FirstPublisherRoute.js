'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import PublisherList from './PublisherList';

class FirstPublisherRoute extends Component {
  render() {
    return (
      <View style={styles.flex}>

      	<View style={styles.navigatorBar}>
      		<Text style={styles.thePublisher}>
      		  The Publisher List
      		</Text>
      	</View>

      	<View style={styles.pushGridView}>
      		<PublisherList/>
      	</View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
	flex:{
		flex:1,
		backgroundColor:'rgb(247,247,247)',
	},
	navigatorBar:{
		backgroundColor:'#FFFFFF',
		height:45,
		width:window.width,
		borderBottomWidth:1,
		borderColor:'rgb(221,221,221)',
	},
	thePublisher:{
		marginTop:10,
		fontSize:18,
		color:'rgb(67,67,67)',
		alignSelf:'center',
		fontWeight:'bold',
	},
	pushGridView:{
		flex:1,
		marginTop:4,
	},

});


export default FirstPublisherRoute;