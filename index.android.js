/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


  //  Image json form:

  //  <Image
  //   source={{uri: movie.posters.thumbnail}}
  //   style={styles.thumbnail}/> 

'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  StyleSheet,
  View,
} from 'react-native';
import UserProfile from './UserProfile';
import ScrollableTabView, { DefaultTabBar,ScrollableTabBar } from 'react-native-scrollable-tab-view';
import UserProfilePublisher from './UserProfilePublisher';
import FirstFeedRoute from './FirstFeedRoute';
import FirstPublisherRoute from './FirstPublisherRoute';

class Publisher extends Component {

  render() {

    return (

              <ScrollableTabView style={{marginTop:20}}
                                 tabBarBackgroundColor='#F7F7F7'
                                 locked={true}
                                 tabBarUnderlineColor='#FFFFFFFF'
                                 scrollWithoutAnimation={true}
                                 tabBarPosition='top'
                                 initialPage={0}
                                 tabBarActiveTextColor='#434343'
                                 tabBarInactiveTextColor='rgba(67, 67, 67,0.3)'
                                 renderTabBar={() => <ScrollableTabBar/>}>
                 <FirstFeedRoute tabLabel="Feed" />
                 <FirstPublisherRoute tabLabel="Publisher"/>
                 <UserProfile tabLabel="Me" />
                 <UserProfilePublisher tabLabel="Test"/>
                 {/*<Publish tabLabel="Publisher" />
                 <PublishColumn tabLabel="column"/>
                 <SimpleNavigationApp tabLabel="Article"/>*/}
              </ScrollableTabView>
    );
  }
}


const styles = StyleSheet.create({

  actionBar:{
    width:window.width,
  },

});
AppRegistry.registerComponent('Publisher', () => Publisher);