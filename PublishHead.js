'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  AppRegistry,
  Text,
  StyleSheet,
  Image,
  View,
} from 'react-native';

class PublishHead extends Component {
  render() {
    return (

      <View>	

      	{/*cover image*/}
     		<Image
         			style={styles.cover}
        			source={require('./img/publishCover.jpg')}/>

        {/*position absolute mock view*/}
        <View style={styles.mockProfilePic}>
					     <Image 
						    style={styles.publisherLogo}
						    source={require('./img/logo.jpg')}/> 
        </View>
        		
        {/*history writter*/}
				<View style={styles.publisherHistory}>

					<View>
			    		<View style={styles.firstRow}>
							  
			    		</View>

			    		<View style={styles.secondRow}>
							  
			     		</View>
					</View>

					<View style={styles.publisherName}>
							<Text style={{fontSize:18}}>
								Skull Candy
							</Text>
					</View>

					<View style={styles.publisherDescription}>
							<Text style={{fontSize:14,color:'#262626'}} numberOfLines={3}>
								Lorem ipsum dolor sit amet,  quis nostrud exercitation ullamco. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</Text>
					</View>
				</View>
					
				{/*layout height:8 */}
				<View style={styles.singleLine}/> 

      </View>
    );
  }
}

var window = Dimensions.get('window');

const styles = StyleSheet.create({

	contrainer:{
		backgroundColor:'#dddddd',
	},
	cover:{
		zIndex:1,
		height:130,
		width: window.width,
	},
	publisherHistory:{
		zIndex:1,
		flexDirection: 'column',
    justifyContent: 'space-between',
		height:174,
		width: window.width,

	},
	publisherLogo:{
		borderRadius:10,
		width:128,
		height:128,
		borderWidth:2,
		borderColor:'#dddddd',
	},
  singleLine:{

    marginTop:10,
    height:8,
    width:window.width,
    borderWidth:1,
    borderColor:'#e6e6e6',
    backgroundColor:'#f2f2f2',
  },
  views:{
  	textAlign:'center',
  	fontSize:24,
		marginTop:10,
 		width:102,
  },
  articles:{
  		textAlign:'center',
  		fontSize:24,
		marginTop:10,
 		width:98,	
  },
  firstRow:{
    	flexDirection: 'row',
   	 	justifyContent: 'space-between',
  },
  secondRow:{
    	flexDirection: 'row',
    	justifyContent: 'space-between',
  },
  textViews:{
  	textAlign:'center',
  		marginLeft:35,
  		fontSize:14,
  		color:'#8C8887',
  },
  textArticles:{
  		fontSize:14,
  		color:'#8C8887',
  		marginRight:30,
  },

  publisherDescription:{
  		paddingRight:20,
  		paddingLeft:20,
      marginBottom:20,
  },
  publisherName:{
  		marginTop:65,
  		alignItems:'center',
  },
  mockProfilePic:{
  		zIndex:4,
  		height: 0,
		width: window.width,
		alignItems: 'center',
		backgroundColor: '#DDFFDD',
		position: 'absolute',
		top: 64,
  },
});

export default PublishHead;