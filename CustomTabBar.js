'use strict';

import React, { Component } from 'react';

import {
  TabBarIOS,
  StyleSheet,
  View,
} from 'react-native';


class CustomTabBar extends Component {
	constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'feed'
    };
  }
  render() {
    return (
      <TabBarIOS 
         unselectedTintColor="grey"
         tintColor="white"
         barTintColor="#dddddd"  
         selectedTab={this.state.selectedTab}>

        <TabBarIOS.Item
          selected={this.state.selectedTab === 'feed'}
          systemIcon="contacts"
          onPress={() => {
              this.setState({
                  selectedTab: 'feed',
              });
            }}>
            <Feed/>
        </TabBarIOS.Item>

        <TabBarIOS.Item
          selected={this.state.selectedTab === 'column'}
          systemIcon="featured"
          onPress={() => {
                this.setState({
                    selectedTab: 'column',
                });
          }}>
          <Publish/>
        </TabBarIOS.Item>


        <TabBarIOS.Item
          selected={this.state.selectedTab === 'row'}
          systemIcon="bookmarks"
          onPress={() => {
                this.setState({
                    selectedTab: 'row',
                });
          }}>
          <Writter/>
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
}

const styles = StyleSheet.create({

});

export default CustomTabBar;