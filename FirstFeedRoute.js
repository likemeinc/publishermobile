'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Navigator,
  Dimensions,
  Text,
  ScrollView,
} from 'react-native';
import FeedList from './FeedList';
import Article from './Article';
import PublishColumn from './PublishColumn';

class FirstFeedRoute extends Component {

  render() {
    return (
      <View style={styles.flex}>

      	<View style={styles.navigatorBar}>
      		<Text style={styles.thePublisher}>
      		  The Publisher
      		</Text>
      	</View>
      	<FeedList/>
      </View>
    );
  }

}

var window = Dimensions.get('window');
const styles = StyleSheet.create({

	flex:{
		flex:1,
	},

	navigatorBar:{
		backgroundColor:'#FFFFFF',
		height:45,
		width:window.width,
		borderBottomWidth:1,
		borderColor:'rgb(221,221,221)',
	},
	thePublisher:{
		marginTop:10,
		fontSize:18,
		color:'rgb(67,67,67)',
		alignSelf:'center',
		fontWeight:'bold',
	},

});


export default FirstFeedRoute;